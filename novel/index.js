module.exports = {
    toolbar: {
        //version short : implementation is in ToolbarDefaultConfig
        exports: ['pdf',
            {
                type: 'epub',
                icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QgXCQIUy0/LEAAAAXdJREFUOMt9kz1LnEEQx3+5WBgMYrVil9peQUFRQae2knhik0ttEAVhNCq6WKhwjZ1d9MCPsAi+EDglwhX5AHY20wtCUtjMI3vno0+zO/+X2dl5ZuGdL6awHFNYfk9TKTEV6ylwCBzGFE5y7s0EMQVUjJhCHZjPqGpMoe5cW4IPJeZj4JvDF75O+XqsYt8L7UuCN8wNFas63wC+Ov5LxRbLKqgDSx7+VrFxx3uBB+CzcyMqdlv4ulx0AlRLTu4H/mbmmcIcU1gATj/GFBT44YJzFZt1wQBwD/QB/4ExFbt2bg04Av5VgMGsqTfZ/gvwyffTKtZ08xaw5/hgBagBLQd+xhRWAFTsBpgAJlTsys0rwKZrW0Atb2ITGPFwV8U2OmZkB1gvKlWx0bbfCPQAl8CQi1ZV7CA7ed/xO2ASeFSx0kH6kyUpyt0uzCo2/GqQOt5BD3CeXSdv8HRxculbcOLR79fKqJZjbeZXFXRU0w2ceTinYk9lumcKvJzSZsenBAAAAABJRU5ErkJggg==',
                tooltip: 'Exporter en EPUB',
                format: 'epub',
                process: function () {
                    electron.exportEpub();
                }
            }],
        inlines: ['important', 'quote'],
        sections: ['normal', 'header', 'olist', 'ulist'],
        divisions: ['blockquote'],
        inserts: ['image', 'attachment']
    },
    model: {
        decorators: ['important', 'quote'],
        sections: ['hierarchical', 'olist', 'ulist', 'image', 'attachment'],
        divisions: ['blockquote']
    },
    language: 'FR'
};
